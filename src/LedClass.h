/*
 * LedClass.h
 *
 *  Created on: 26 May 2015
 *      Author: Namcho
 */

#ifndef LEDCLASS_H_
#define LEDCLASS_H_

//BLDC driver board

//#define LED1	GPIO_Pin_4
//#define LED2	GPIO_Pin_5
//#define LED3	GPIO_Pin_6
//#define LED4	GPIO_Pin_7

//STM32F3Disco  Led definations
#define LED1	GPIO_Pin_9
#define LED2	GPIO_Pin_10
#define LED3	GPIO_Pin_11
#define LED4	GPIO_Pin_12


//#define GPIOxCLOCK		RCC_AHBPeriph_GPIOE
//#define GPIOx			GPIOE


#include "TaskOOP.h"
#include "IGpio.h"

class LedClass : public TaskOOP , public IGpio{
public:
	LedClass():_whLed(0), _taskTime(0){}
	LedClass(uint16_t LEDx, uint16_t taskTimeMs);
	virtual ~LedClass(){_whLed = 0;}
	virtual void setPort(uint16_t portVal);
	virtual uint16_t getPort(void);
	virtual void setPin(uint16_t LEDx, bool pinVal);
	virtual bool getPin(void);
private:
	uint16_t _whLed;
	uint16_t _taskTime;
	virtual void initPort(void);

protected:
	virtual void run();
};

#endif /* LEDCLASS_H_ */
