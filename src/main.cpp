//
// This file is part of the GNU ARM Eclipse distribution.
// Copyright (c) 2014 Liviu Ionescu.
//

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include "diag/Trace.h"
#include "LedClass.h"
//#include "Hc05.h"
#include "Hc05Stm.h"
#include "Bluetooth.h"

// ----- main() ---------------------------------------------------------------

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"

extern "C"{
	void SysTick_Handler(void);
}

static	LedClass ledObj1(LED1, 200);
static	LedClass ledObj2(LED2, 400);
static  LedClass ledObj3(LED3, 600);
static	LedClass ledObj4(LED4, 800);
static 	Hc05Stm	hc05Obj(45,25,Hc05Base::MODES_SLAVE);
static	Bluetooth blueObj(800, &hc05Obj);

int
main(int argc, char* argv[])
{


	//ledObj1.create("LedTask1",configMINIMAL_STACK_SIZE,1);

	//Systick init'i kur.
	SystemInit();
	//SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);
	if (SysTick_Config(SystemCoreClock / 1000))
	{
	  /* Capture error */
	  while (1);
	}
	ledObj2.create("LedTask2",configMINIMAL_STACK_SIZE,2);
	ledObj3.create("LedTask3",configMINIMAL_STACK_SIZE,3);
	ledObj4.create("LedTask4",configMINIMAL_STACK_SIZE,4);
	//Bluetooth.create("BlueTooth", configMINIMAL_STACK_SIZE, 1);
	blueObj.create("BlueTooth", configMINIMAL_STACK_SIZE, 1);

	TaskOOP::taskStartScheduler();

	while (1)
	{

	}
}

void SysTick_Handler(void){

	TaskOOP::portSysTickHandler();

}

#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
