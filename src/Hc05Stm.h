/*
 * Hc05Stm.h
 *
 *  Created on: 2 Tem 2015
 *      Author: NAZIM YILDIZ
 */

#ifndef HC05STM_H_
#define HC05STM_H_

#include "Hc05Base.h"

class Hc05Stm:public Hc05Base {
public:
	Hc05Stm(uint8_t transmitBuffersize, uint8_t receiveBufferSize, Hc05Base::Modes mode);
	virtual ~Hc05Stm();

	virtual void comSendTest(int8_t *message, uint8_t messageLen);
private:

protected:
	virtual void sendBytes();
	virtual void receiveBytes();
	virtual void hardwareInit();
	virtual void listenRoutine();
	virtual void talkingRoutine();
	virtual void goATCommandMode();
	virtual void errorState();
};

#endif /* HC05STM_H_ */
