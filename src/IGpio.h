/*
 * IGpio.h
 *
 *  Created on: 26 May 2015
 *      Author: hasip.tuna
 */

#ifndef IGPIO_H_
#define IGPIO_H_

typedef unsigned short uint16_t;

//Template kullanılarak yazılcak.
class IGpio {
public:
	IGpio(){}
	virtual ~IGpio(){}

	virtual void setPort(uint16_t portVal) = 0;
	virtual uint16_t getPort(void) = 0;

	virtual void setPin(uint16_t LEDx, bool pinVal) = 0;
	virtual bool getPin(void) = 0;
private:
	virtual void initPort(void) = 0;
protected:

};

#endif /* IGPIO_H_ */






