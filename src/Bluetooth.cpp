/*
 * Bluetooth.cpp
 *
 *  Created on: 3 Tem 2015
 *      Author: NAZIM YILDIZ
 */

#include "Bluetooth.h"
#include "stm32f30x_gpio.h"

Bluetooth::Bluetooth(uint16_t taskTimeMs, Hc05Stm *hc05Obj) {
	// TODO Auto-generated constructor stub
	_hc05Obj = hc05Obj;
	_taskTimingMs = taskTimeMs;
}

Bluetooth::~Bluetooth() {
	// TODO Auto-generated destructor stub

}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void Bluetooth::run(){
	uint8_t led;
	int8_t msg[7] = "ENKO\r\n";

	led = 0;

	for(;;){
		_hc05Obj->comSendTest((int8_t *)&msg[0],6);
		led = led ^ 0x01;
		if(led)
			GPIO_WriteBit(GPIOE,GPIO_Pin_9, Bit_SET);
		else
			GPIO_WriteBit(GPIOE,GPIO_Pin_9, Bit_RESET);

		TaskOOP::delay(_taskTimingMs);
	}
}
