/*
 * Hc05.h
 *
 *  Created on: Jun 27, 2015
 *      Author: Namcho
 */

#ifndef HC05BASE_H_
#define HC05BASE_H_

#include "stdint.h"

class Hc05Base {
public:
	enum Baudrate{BAUD_9600, BAUD_19200, BAUD_38400, BAUD_57600, BAUD_115200};
	enum Stopbits{STOP_1, STOP_2};
	enum Parity{PARITY_NONE, PARITY_ODD, PARITY_EVEN};
	enum Modes{MODES_MASTER, MODES_SLAVE};
	enum States{BASECONFIG, TALKING, ERROR, WAITING, PAIRING};
	Hc05Base(uint8_t transmitBuffersize, uint8_t receiveBufferSize, Hc05Base::Modes mode);
	virtual ~Hc05Base();

	void setBaud(Hc05Base::Baudrate baud);
	void setStopbit(Hc05Base::Stopbits stopbit);
	void setParity(Hc05Base::Parity parity);
	void setMode(Hc05Base::Modes mode);
	void setState(Hc05Base::States state);
	void atMode();

	void setName(int8_t const *name, uint8_t len);
	void setPassword(int8_t const *pass, uint8_t len);

private:
	int8_t *_transmitBuffer;			//Dinamik
	int8_t *_receiveBuffer;				//
	uint8_t _transmitBufferLen;
	uint8_t _receiveBufferLen;
	uint8_t _transmitBufferIndex;
	uint8_t _receiveBufferIndex;

	int8_t 				_password[6];
	int8_t 				_name[6];
	enum Baudrate 		_baud;
	enum Stopbits 		_stopbit;
	enum Parity   		_parity;
	enum Modes			_mode;
	enum States		 	_state;

protected:
	bool addItemToTransmitBuffer(int8_t item);
	virtual void sendBytes() = 0;
	virtual void receiveBytes() = 0;
	virtual void hardwareInit() = 0;
	virtual void listenRoutine() = 0;
	virtual void talkingRoutine() = 0;
	virtual void goATCommandMode() = 0;
	virtual void errorState() = 0;

	virtual uint8_t getTransmitBufferIndex();
	virtual uint8_t getTransmitBufferLen();
	virtual int8_t 	getTransmitBufferVal(uint8_t index);
	virtual uint8_t getReceiveBufferIndex();
	virtual uint8_t getReceiveBufferLen();
	virtual int8_t getReceiveBufferVal(uint8_t index);
	virtual uint32_t getTransmitBufferAddress();
	virtual uint32_t getReceiveBufferAddress();

	virtual void setTransmitBufferIndex(uint8_t val);
	virtual void setTransmitBufferVal(int8_t val, uint8_t index);
	virtual void setReceiveBufferIndex(uint8_t val);
	virtual void setReceiveBufferVal(int8_t val, uint8_t index);
};

#endif /* HC05BASE_H_ */

