/*
 * TaskOOP.cpp
 *
 *  Created on: 25 May 2015
 *      Author: Namcho
 */

#include "TaskOOP.h"

extern "C"{
	extern void xPortSysTickHandler();
}

TaskOOP::TaskOOP():_handle(0) {
    // TODO Auto-generated constructor stub

}

TaskOOP::~TaskOOP() {
    // TODO Auto-generated destructor stub
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void TaskOOP::create(const char * const pcName, const uint16_t usStackDepth, UBaseType_t uxPriority){

    xTaskCreate(bridge,pcName,usStackDepth,this,uxPriority,&_handle);
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void TaskOOP::deleteT(){

    vTaskDelete(_handle);
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void TaskOOP::suspend(){

    vTaskSuspend(_handle);
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void TaskOOP::resume(){

    vTaskResume(_handle);
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
UBaseType_t  TaskOOP::getTaskPriority(){

    return uxTaskPriorityGet(_handle);
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void TaskOOP::setTaskPriority(UBaseType_t uxNewPriority){

    vTaskPrioritySet(  _handle,  uxNewPriority );
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void TaskOOP::delay(const TickType_t xTicksToDelay){

    vTaskDelay(xTicksToDelay);
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void TaskOOP::taskStartScheduler(void){

    vTaskStartScheduler();
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void TaskOOP::taskEndScheduler( ){

    vTaskEndScheduler();
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void TaskOOP::taskSuspendAll(void){

    vTaskSuspendAll();
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
BaseType_t TaskOOP::taskResumeAll( void ){

    return xTaskResumeAll();
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void TaskOOP::portSysTickHandler(){
	xPortSysTickHandler();
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void TaskOOP::bridge(void* pThis){
    TaskOOP* task = (TaskOOP *)pThis;
    task->run();
}
