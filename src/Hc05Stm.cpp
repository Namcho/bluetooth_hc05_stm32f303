/*
 * Hc05Stm.cpp
 *
 *  Created on: 2 Tem 2015
 *      Author: hasip.tuna
 */

#include "Hc05Stm.h"
#include "stm32f30x_gpio.h"
#include "stm32f30x_usart.h"
#include "stm32f30x_dma.h"
#include "stm32f30x_rcc.h"

Hc05Stm::Hc05Stm(uint8_t transmitBuffersize, uint8_t receiveBufferSize, Hc05Base::Modes mode):
Hc05Base(transmitBuffersize, receiveBufferSize, mode){
	// TODO Auto-generated constructor stub
	hardwareInit();
}

Hc05Stm::~Hc05Stm() {
	// TODO Auto-generated destructor stub
}
void Hc05Stm::hardwareInit(){
	GPIO_InitTypeDef	gpiox;
	USART_InitTypeDef 	usart;
	DMA_InitTypeDef		dma;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA,ENABLE);
	//PA9 - TX, PA10 - RX
	gpiox.GPIO_Mode 	= GPIO_Mode_AF;
	gpiox.GPIO_OType 	= GPIO_OType_PP;
	gpiox.GPIO_Pin 		= GPIO_Pin_9 | GPIO_Pin_10;
	gpiox.GPIO_PuPd 	= GPIO_PuPd_UP;
	gpiox.GPIO_Speed 	= GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &gpiox);

	//Alternatif fonksiyon atamasi
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_7);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_7);

	//DMA_init Transmit
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	dma.DMA_BufferSize				= getTransmitBufferLen();
	dma.DMA_DIR						= DMA_DIR_PeripheralDST;
	dma.DMA_M2M						= DMA_M2M_Disable;
	dma.DMA_MemoryBaseAddr			= getTransmitBufferAddress();
	dma.DMA_MemoryDataSize			= DMA_MemoryDataSize_Byte;
	dma.DMA_MemoryInc				= DMA_MemoryInc_Enable;
	dma.DMA_Mode					= DMA_Mode_Normal;
	dma.DMA_PeripheralBaseAddr		= (uint32_t)&USART1->TDR;
	dma.DMA_PeripheralDataSize		= DMA_PeripheralDataSize_Byte;
	dma.DMA_PeripheralInc			= DMA_PeripheralInc_Disable;
	dma.DMA_Priority				= DMA_Priority_Low;
	DMA_Init(DMA1_Channel4, &dma);
	DMA_Cmd(DMA1_Channel4, ENABLE);

	//DMA_init Recieve
	dma.DMA_BufferSize				= getReceiveBufferLen();
	dma.DMA_DIR						= DMA_DIR_PeripheralSRC;
	//dma.DMA_Mode					= DMA_Mode_Circular;
	dma.DMA_MemoryBaseAddr			= getReceiveBufferAddress();
	dma.DMA_PeripheralBaseAddr		= (uint32_t)&USART1->RDR;
	dma.DMA_Priority				= DMA_Priority_Medium;
	DMA_Init(DMA1_Channel5, &dma);
	DMA_Cmd(DMA1_Channel5, ENABLE);

	//USART init
	RCC_APB2PeriphClockCmd(RCC_APB2ENR_USART1EN, ENABLE);
	usart.USART_BaudRate			= 38400;
	usart.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	usart.USART_Mode				= USART_Mode_Tx | USART_Mode_Rx;
	usart.USART_Parity				= USART_Parity_No;
	usart.USART_StopBits			= USART_StopBits_1;
	usart.USART_WordLength			= USART_WordLength_8b;
	USART_Init(USART1, &usart);
	USART_Cmd(USART1,ENABLE);
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void Hc05Stm::sendBytes(){
	if(USART_GetFlagStatus(USART1, USART_FLAG_TC) == SET){
		DMA_Cmd(DMA1_Channel4, DISABLE);
		USART_DMACmd(USART1, USART_DMAReq_Tx, DISABLE);
		USART_ClearFlag(USART1, USART_FLAG_TC);

		DMA_SetCurrDataCounter(DMA1_Channel4, getTransmitBufferIndex());
		DMA_Cmd(DMA1_Channel4,ENABLE);
		USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);
		setTransmitBufferIndex(0);
	}
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void Hc05Stm::receiveBytes(){

	USART_DMACmd(USART1, USART_DMAReq_Rx, DISABLE);
	DMA_Cmd(DMA1_Channel5, DISABLE);
	DMA_SetCurrDataCounter(DMA1_Channel5,getReceiveBufferLen());
	DMA_Cmd(DMA1_Channel5, ENABLE);
	USART_DMACmd(USART1, USART_DMAReq_Rx, ENABLE);
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void Hc05Stm::listenRoutine(){
	uint8_t commandStartIndex;
	uint8_t commandEndIndex;
	uint8_t isItCommand;

	commandStartIndex 	= 0;
	commandEndIndex 	= 0;
	isItCommand 		= 0;
	for(int index = 1; index < getReceiveBufferLen(); index++){
		if((getReceiveBufferVal(index) == 'x') & (getReceiveBufferVal(index - 1) == 'y')){
			isItCommand			= 1;
			commandEndIndex		= index;
			if((getReceiveBufferVal(commandStartIndex) == 'A') & (getReceiveBufferVal(commandStartIndex + 1) == 'B')){
				addItemToTransmitBuffer('A');
				addItemToTransmitBuffer('B');
				addItemToTransmitBuffer(' ');
				//addItemToTransmitBuffer('\n');
			}
			else if(getReceiveBufferVal(commandStartIndex) == 'A'){
				addItemToTransmitBuffer('A');
				addItemToTransmitBuffer(' ');
				//addItemToTransmitBuffer('\n');
			}
			else if(getReceiveBufferVal(commandStartIndex) == 'B'){
				addItemToTransmitBuffer('B');
				addItemToTransmitBuffer(' ');
				//addItemToTransmitBuffer('\n');
			}
			else if(getReceiveBufferVal(commandStartIndex) == 'C'){
				addItemToTransmitBuffer('C');
				addItemToTransmitBuffer(' ');
				//addItemToTransmitBuffer('\n');
			}
			else{
				//Gecersiz komut...
			}

			commandStartIndex	= commandEndIndex + 1;

		}
	}

	//Komut mesaji degil ise aldigini aynen gonder
	if(isItCommand == 0){
		for(int index = 0; index < getReceiveBufferLen(); index++){
			addItemToTransmitBuffer(getReceiveBufferVal(index));
		}
	}

	//Alinan mesaj� temizle
	for(int index = 0; index < getReceiveBufferLen(); index++){
		setReceiveBufferVal(0, index);
	}
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void Hc05Stm::talkingRoutine(){

}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void Hc05Stm::goATCommandMode(){

}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void Hc05Stm:: errorState(){

}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void Hc05Stm::comSendTest(int8_t *message, uint8_t messageLen){

	uint8_t emptyCells;

	listenRoutine();
	receiveBytes();

	//Herhangi bir devam eden islem yok ise
	emptyCells = getTransmitBufferLen() - getTransmitBufferIndex();
	if(emptyCells > messageLen){

		for(int var = 0; var < messageLen; var++){
			addItemToTransmitBuffer(*message);
			message++;
		}
	}

	sendBytes();
}
