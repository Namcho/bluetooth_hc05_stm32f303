/*
 * Hc05.cpp
 *
 *  Created on: Jun 27, 2015
 *      Author: Namcho
 */


#include "Hc05Base.h"

Hc05Base::Hc05Base(uint8_t transmitBuffersize, uint8_t receiveBufferSize, Hc05Base::Modes mode) {
	// TODO Auto-generated constructor stub

	_transmitBuffer = new int8_t[transmitBuffersize];
	_receiveBuffer  = new int8_t[receiveBufferSize];

	_transmitBufferLen = transmitBuffersize;
	_receiveBufferLen  = receiveBufferSize;

	this->_state 	= 	States::TALKING;
	this->_baud		=	BAUD_38400;
	this->_stopbit	=	STOP_1;
	this->_parity	=	PARITY_NONE;
	this->_mode		=	mode;

	this->_receiveBufferIndex	=	0;
	this->_transmitBufferIndex	=	0;
}

Hc05Base::~Hc05Base() {
	// TODO Auto-generated destructor stub
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void Hc05Base::setBaud(Hc05Base::Baudrate baud){
	this->_baud = baud;
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void Hc05Base::setParity(Hc05Base::Parity parity){
	this->_parity = parity;
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void Hc05Base::setStopbit(Hc05Base::Stopbits stopbit){
	this->_stopbit = stopbit;
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void Hc05Base::setName(int8_t const *name, uint8_t len){

	for (uint8_t var = 0; var < len; ++ var) {
		this->_name[var] = *name;
		name++;
	}

}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void Hc05Base::setPassword(int8_t const *pass, uint8_t len){

	for (uint8_t var = 0; var < len; ++var) {
		this->_password[var] = *pass;
		pass++;
	}
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void Hc05Base::setMode(Hc05Base::Modes mode){
	this->_mode = mode;
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
bool Hc05Base::addItemToTransmitBuffer(int8_t item){

	//Buffer boyutu asilmis ise ekleme yapma
	if(this->_transmitBufferIndex >= this->_transmitBufferLen)
		return false;

	this->_transmitBuffer[this->_transmitBufferIndex]	= item;
	this->_transmitBufferIndex++;
	return true;
}
/*
 * @brief         : Get methodlari
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
uint8_t Hc05Base::getTransmitBufferIndex(){
	return this->_transmitBufferIndex;
}
uint8_t Hc05Base::getTransmitBufferLen(){
	return this->_transmitBufferLen;
}
uint8_t Hc05Base::getReceiveBufferIndex(){
	return this->_receiveBufferIndex;
}
uint8_t Hc05Base::getReceiveBufferLen(){
	return this->_receiveBufferLen;
}
int8_t 	Hc05Base::getTransmitBufferVal(uint8_t index){
	return this->_transmitBuffer[index];
}
int8_t Hc05Base::getReceiveBufferVal(uint8_t index){
	return this->_receiveBuffer[index];
}
uint32_t Hc05Base::getTransmitBufferAddress(){
	return (uint32_t)&this->_transmitBuffer[0];
}
uint32_t Hc05Base::getReceiveBufferAddress(){
	return (uint32_t)&this->_receiveBuffer[0];
}
/*
 * @brief         : Set methodlari
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void Hc05Base::setTransmitBufferIndex(uint8_t val){
	this->_transmitBufferIndex = val;
}
void Hc05Base::setTransmitBufferVal(int8_t val, uint8_t index){
	this->_transmitBuffer[index] = val;
}
void Hc05Base::setReceiveBufferIndex(uint8_t val){
	this->_receiveBufferIndex = val;
}
void Hc05Base::setReceiveBufferVal(int8_t val, uint8_t index){
	this->_receiveBuffer[index] = val;
}
