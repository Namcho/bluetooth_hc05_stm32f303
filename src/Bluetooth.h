/*
 * Bluetooth.h
 *
 *  Created on: 3 Tem 2015
 *      Author: hasip.tuna
 */

#ifndef BLUETOOTH_H_
#define BLUETOOTH_H_
#include "TaskOOP.h"
#include "Hc05Stm.h"

class Bluetooth:public TaskOOP {
public:
	Bluetooth(uint16_t taskTimeMs, Hc05Stm *hc05Obj);
	virtual ~Bluetooth();

private:
	uint16_t _taskTimingMs;
protected:
	Hc05Stm	*_hc05Obj;
	virtual void run();
};

#endif /* BLUETOOTH_H_ */
