/*
 * LedClass.cpp
 *
 *  Created on: 26 May 2015
 *      Author: Namcho
 */

#include "LedClass.h"
#include "stm32f30x.h"
#include "stm32f30x_rcc.h"
#include "stm32f30x_gpio.h"


LedClass::LedClass(uint16_t LEDx, uint16_t taskTimeMs) {
	// TODO Auto-generated constructor stub
	_whLed = LEDx;
	_taskTime = taskTimeMs;
	initPort();
}

//LedClass::~LedClass() {
//	// TODO Auto-generated destructor stub
//	//Port deinit yapilcak
//}

void LedClass::setPort(uint16_t val){
	GPIO_Write(GPIOE, val);
}

uint16_t LedClass::getPort(void){

	return GPIO_ReadInputData(GPIOE);
}

void LedClass::setPin(uint16_t LEDx, bool pinVal){
	if(pinVal == true)
		GPIO_WriteBit(GPIOE,LEDx, Bit_SET);
	else
		GPIO_WriteBit(GPIOE,LEDx, Bit_RESET);
}

bool LedClass::getPin(void){

	return false;
}

void LedClass::initPort(void){
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOE,ENABLE);
	GPIO_InitTypeDef gpInit;
	gpInit.GPIO_Mode = GPIO_Mode_OUT;
	gpInit.GPIO_OType = GPIO_OType_PP;
	gpInit.GPIO_PuPd = GPIO_PuPd_NOPULL;
	gpInit.GPIO_Speed = GPIO_Speed_2MHz;
	gpInit.GPIO_Pin =  _whLed;
	GPIO_Init(GPIOE, &gpInit);
}

void LedClass::run(void){
	uint8_t led=0;

	for(;;){
		led = led ^ 0x01;

		if(led){
			setPin(_whLed,true);

		}
		else{
			setPin(_whLed,false);
		}

		TaskOOP::delay(_taskTime);
	}
}

