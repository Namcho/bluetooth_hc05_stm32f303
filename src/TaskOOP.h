/*
 * TaskOOP.h
 *
 *  Created on: 25 May 2015
 *      Author: Namcho
 */

#ifndef  TASKOOP_H_
#define TASKOOP_H_

extern "C"{
#include "FreeRTOS.h"
#include "task.h"
}

class TaskOOP {
private:
    TaskHandle_t _handle;
public:
    TaskOOP();
    virtual ~TaskOOP();
    void create(const char * const pcName, const uint16_t usStackDepth, UBaseType_t uxPriority);    //xTaskCreate( pvTaskCode, pcName, usStackDepth, pvParameters, uxPriority, pxCreatedTask )
    void deleteT(); //vTaskDelete( TaskHandle_t xTaskToDelete )
    void suspend(); //vTaskSuspend( TaskHandle_t xTaskToSuspend )
    void resume();  //vTaskResume( TaskHandle_t xTaskToResume )
    UBaseType_t getTaskPriority();                      //uxTaskPriorityGet( TaskHandle_t xTask )
    void setTaskPriority(UBaseType_t uxNewPriority);    //vTaskPrioritySet( TaskHandle_t xTask, UBaseType_t uxNewPriority )

    static void delay(const TickType_t xTicksToDelay);  //vTaskDelay( const TickType_t xTicksToDelay )
    static void taskStartScheduler(  );
    static void taskEndScheduler( void );
    static void taskSuspendAll( void );
    static BaseType_t taskResumeAll( void );
    //FreeRTOS's tick function will wrapped to use SysTick_Handler() for others like HAL_Tick, Graphic LCD's,.. .. etc tick routines.
    static void portSysTickHandler();
private:
    static void bridge(void* pThis);
protected:
    virtual void run(void) = 0;

};

#endif /* TASKOOP_H_ */

